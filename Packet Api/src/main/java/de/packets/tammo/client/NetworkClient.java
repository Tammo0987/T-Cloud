package de.packets.tammo.client;

import de.packets.tammo.logging.NettyLogger;
import de.packets.tammo.packet.Packet;
import de.packets.tammo.packet.PacketDecoder;
import de.packets.tammo.packet.PacketEncoder;
import de.packets.tammo.packet.PacketHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NetworkClient {

    /**
     * Host Adress to connect
     */
    private final String host;

    /**
     * Port to connect
     */
    private final int port;

    /**
     * Epoll is availiable
     */
    private final boolean EPOLL = Epoll.isAvailable();

    /**
     * Worker EventLoopGroup
     */
    private EventLoopGroup workerGroup;

    /**
     * PacketHandler to handle and send Packets
     */
    private PacketHandler packetHandler = new PacketHandler();


    /**
     * NetworkClient Constructor
     *
     * @param host Host Adress
     * @param port Port
     */
    public NetworkClient(final String host, final int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * Connect to the Server
     *
     * @return NetworkClient Instance
     */
    public NetworkClient connect() {
        new Thread(this::run).start();
        return this;
    }

    /**
     * Send a Packet
     *
     * @param packet Packet
     */
    public void sendPacket(final Packet packet) {
        this.packetHandler.sendPacket(packet);
    }

    /**
     * Disconnect from the Server
     */
    public void disconnect() {
        this.workerGroup.shutdownGracefully();
        NettyLogger.log("Disconnected...");
    }

    private void run() {
        this.workerGroup = this.EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();

        try {
            final Bootstrap bootstrap = new Bootstrap()
                    .group(workerGroup)
                    .channel(this.EPOLL ? EpollSocketChannel.class : NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {

                        @Override
                        protected void initChannel(final SocketChannel channel) throws Exception {
                            channel.pipeline().addLast(new PacketDecoder()).addLast(new PacketEncoder()).addLast(NetworkClient.this.packetHandler);
                            NetworkClient.this.packetHandler.setHost(channel.remoteAddress().getAddress().getHostAddress());
                        }

                    });

            final ChannelFuture future = bootstrap.connect(this.host, this.port).sync();

            NettyLogger.log("Connected to " + this.host + " on Port " + this.port);

            Runtime.getRuntime().addShutdownHook(new Thread(this::disconnect));

            future.channel().closeFuture().sync();
        } catch (Exception e) {
            NettyLogger.logException("Cant connect to " + this.host + " on Port " + this.port, e);
        } finally {
            this.workerGroup.shutdownGracefully();
            NettyLogger.log("Disconnected...");
        }
    }
}
