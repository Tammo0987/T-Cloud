package de.packets.tammo.server;

import de.packets.tammo.logging.NettyLogger;
import de.packets.tammo.packet.PacketDecoder;
import de.packets.tammo.packet.PacketEncoder;
import de.packets.tammo.packet.PacketHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NetworkServer {

    /**
     * Port to bind
     */
    private final int port;

    /**
     * Epoll available
     */
    private final boolean EPOLL = Epoll.isAvailable();

    /**
     * Boss EventLoopGroup
     */
    private EventLoopGroup bossGroup;

    /**
     * Worker EventLoopGroup
     */
    private EventLoopGroup workerGroup;

    /**
     * NetworkServer Constructor
     *
     * @param port Port
     */
    public NetworkServer(final int port) {
        this.port = port;
    }

    /**
     * Bind the Server on Port
     */
    public NetworkServer bind() {
        new Thread(() -> {
            this.bossGroup = this.EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();
            this.workerGroup = this.EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();

            try {
                final ServerBootstrap bootstrap = new ServerBootstrap()
                        .group(this.bossGroup, this.workerGroup)
                        .channel(this.EPOLL ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
                        .childOption(ChannelOption.SO_KEEPALIVE, true)
                        .childHandler(new ChannelInitializer<SocketChannel>() {

                            @Override
                            protected void initChannel(SocketChannel channel) throws Exception {
                                channel.pipeline().addLast(new PacketEncoder()).addLast(new PacketDecoder()).addLast(new PacketHandler());
                                NettyLogger.log("Client connected from " + channel.remoteAddress().getAddress().getHostAddress());
                            }

                        })
                        .option(ChannelOption.SO_BACKLOG, 128)
                        .childOption(ChannelOption.SO_KEEPALIVE, true);


                final ChannelFuture future = bootstrap.bind(this.port).sync();

                NettyLogger.log("Bind Server on Port " + this.port);
                Runtime.getRuntime().addShutdownHook(new Thread(NetworkServer.this::close));

                future.channel().closeFuture().sync();
            } catch (Exception e) {
                NettyLogger.logException("Cant bind the Server on Port " + this.port, e);
            } finally {
                this.bossGroup.shutdownGracefully();
                this.workerGroup.shutdownGracefully();

                NettyLogger.log("The Server was closed...");
            }

        }).start();
        return this;
    }

    /**
     * Close the Server
     */
    public void close() {
        this.bossGroup.shutdownGracefully();
        this.workerGroup.shutdownGracefully();
        NettyLogger.log("The Server was closed...");
    }

}
