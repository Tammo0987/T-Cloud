package de.packets.tammo.packets;

import de.packets.tammo.packet.Packet;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;

public class SuccessPacket extends Packet {

    /**
     * SuccessPacket Constructor
     */
    public SuccessPacket() {
        super((byte) 0x00);
    }

    public Packet handle(final String host) {
        return null;
    }

    @Override
    public void read(ByteBufInputStream byteBuf) throws IOException {
    }

    @Override
    public void write(ByteBufOutputStream byteBuf) throws IOException {
    }
}
