package de.packets.tammo.packet;

import de.packets.tammo.logging.LogLevel;
import de.packets.tammo.logging.NettyLogger;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.ArrayList;

public class PacketHandler extends SimpleChannelInboundHandler<Packet> {

    /**
     * Channel Connection
     */
    private Channel channel;

    private String host;

    /**
     * Send Packet by init Channel Connection
     */
    private ArrayList<Packet> sendInit = new ArrayList<>();

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.channel = ctx.channel();

        for (final Packet packet : this.sendInit) {
            this.sendPacket(packet);
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet packet) throws Exception {
        final Packet response = packet.handle(this.host);

        if (this.channel == null) {
            return;
        }

        if (response != null) {
            this.channel.writeAndFlush(response);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        NettyLogger.log("Client disconnected...", LogLevel.WARNING);
        NettyLogger.log(cause.getMessage(), LogLevel.WARNING);
    }

    /**
     * Send a Packet
     *
     * @param packet Packet
     */
    public void sendPacket(final Packet packet) {
        if (this.channel == null) {
            this.sendInit.add(packet);
            return;
        }
        this.channel.writeAndFlush(packet);
    }

    public void setHost(final String host) {
        this.host = host;
    }
}