package de.packets.tammo.packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class PacketEncoder extends MessageToByteEncoder<Packet> {

    /**
     * Encodes a Packet
     *
     * @param ctx     ChannelHandlerContext
     * @param packet  Packet to Encode
     * @param byteBuf ByteBuf
     * @throws Exception Exception
     */
    @Override
    protected void encode(ChannelHandlerContext ctx, Packet packet, ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(packet.getId());
        packet.write(new ByteBufOutputStream(byteBuf));
    }

}
