package de.packets.tammo.packet;

import java.util.ArrayList;

public class PacketRegistry {

    /**
     * List of all registered Packets
     */
    private static ArrayList<Packet> packets = new ArrayList<>();

    /**
     * Add a Packet to Registry
     *
     * @param packet Packet
     */
    public static void addPacket(final Packet packet) {
        packets.add(packet);
    }

    /**
     * @param id Packet ID
     * @return Packet Instance by ID
     */
    static Packet getPacketById(final byte id) {
        for (final Packet packet : packets) {
            if (packet.getId() == id) {
                return packet;
            }
        }
        return null;
    }

    /**
     * Remove a Packet from Registry
     *
     * @param packet Packet
     */
    public static void removePacket(final Packet packet) {
        if (packets.contains(packet)) {
            packets.remove(packet);
        }
    }

}
