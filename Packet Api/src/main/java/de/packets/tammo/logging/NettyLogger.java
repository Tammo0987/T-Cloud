package de.packets.tammo.logging;

import java.text.SimpleDateFormat;
import java.util.Date;

public class NettyLogger {

    /**
     * Dateformat for the time
     */
    private final static SimpleDateFormat timeStemp = new SimpleDateFormat("HH:mm:ss");
    /**
     * Prefix before every Message in the Console
     */
    private static String PREFIX = " [Netty Packets] -> ";
    private static boolean logging = true;

    /**
     * Log a Message in the Console
     *
     * @param object   Object to log
     * @param logLevel Level to log
     */
    public static void log(final Object object, final LogLevel logLevel) {
        if (!logging) {
            return;
        }
        final String time = timeStemp.format(new Date());
        switch (logLevel) {
            case INFORMATION:
                System.out.println(time + PREFIX + object.toString());
                break;
            case WARNING:
                System.out.println(time + PREFIX + "[WARNING] " + object.toString());
                break;
            case ERROR:
                System.out.println(time + PREFIX + "[ERROR] " + object.toString());
        }
    }

    /**
     * Do the same as above, but the level is automatically Information
     *
     * @param object Object to log
     */
    public static void log(final Object object) {
        log(object, LogLevel.INFORMATION);
    }

    /**
     * Log a Exception
     *
     * @param error     Error to log
     * @param exception Exception to log
     */
    public static void logException(final Object error, final Exception exception) {
        if (!logging) {
            return;
        }
        log(error.toString(), LogLevel.ERROR);
        log(exception.getMessage(), LogLevel.ERROR);
    }

    public static void setPrefix(final String prefix) {
        NettyLogger.PREFIX = PREFIX;
    }

    public static void setLogging(final boolean logging) {
        NettyLogger.logging = logging;
    }
}
