package de.packets.tammo.logging;

public enum LogLevel {

    /**
     * LogLevel
     */
    INFORMATION,
    WARNING,
    ERROR

}
