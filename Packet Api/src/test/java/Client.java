import de.packets.tammo.client.NetworkClient;
import de.packets.tammo.packet.PacketRegistry;
import de.packets.tammo.packets.SuccessPacket;

public class Client {

    public static void main(String[] args) {
        PacketRegistry.addPacket(new SuccessPacket());
        NetworkClient test = new NetworkClient("localhost", 5000).connect();
        test.sendPacket(new SuccessPacket());
    }

}
