import de.packets.tammo.packet.PacketRegistry;
import de.packets.tammo.packets.SuccessPacket;
import de.packets.tammo.server.NetworkServer;

public class Server {

    public static void main(String[] args) {
        PacketRegistry.addPacket(new SuccessPacket());
        new NetworkServer(5000).bind();
    }

}
