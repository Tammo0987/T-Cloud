package de.tammo.master.utils;

import de.tammo.master.logging.LogLevel;
import de.tammo.master.logging.Logger;
import lombok.Cleanup;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Utils {

    /**
     * @param line String to check
     * @return String is only space
     */
    public static boolean isStringValid(final String line) {
        return line.trim().length() != 0;
    }

    /**
     * Downloads a File
     * @param url Source URL
     * @param filePath File to download
     * @throws IOException ioexception
     */
    public static void download(final String url, final String filePath) throws IOException{
        Logger.log("Downloading " + filePath.split("//")[filePath.split("//").length - 1] + "...");
        final URL website = new URL(url);
        final HttpURLConnection http = (HttpURLConnection) website.openConnection();
        http.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        if(http.getResponseCode() == HttpURLConnection.HTTP_OK){
            @Cleanup final ReadableByteChannel rbc = Channels.newChannel(http.getInputStream());
            @Cleanup final FileOutputStream fos = new FileOutputStream(filePath);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            Logger.log("Download successfully!");
        }else{
            Logger.log("Bad download...", LogLevel.ERROR);
        }
    }

}
