package de.tammo.master.command.commands;

import de.tammo.master.Master;
import de.tammo.master.command.ICommand;
import de.tammo.master.logging.Logger;

import java.io.IOException;

@ICommand.CommandInfo(name = "dispatch", aliases = {"d"})
public class DispatchCommand implements ICommand{

    public void execute(final String[] args){
        if(args.length >= 1){
            if(args[0].equalsIgnoreCase("server")){
                Logger.log("No support!");
            }else{
                final StringBuilder commandbuilder = new StringBuilder();
                for(int i = 0; i < args.length; i++){
                    commandbuilder.append(args[i] + " ");
                }
                try {
                    Master.getProxyHandler().dispatch(commandbuilder.toString());
                } catch (IOException e) {
                    Logger.logException("Cant dispatch command " + commandbuilder.toString(), e);
                }
            }
        }else{
            this.printSyntax();
        }
    }

    public void printSyntax(){
        Logger.log("dispatch <command...>");
        Logger.log("dispatch server <servername> <command...>");
    }
}
