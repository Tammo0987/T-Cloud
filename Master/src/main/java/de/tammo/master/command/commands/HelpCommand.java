package de.tammo.master.command.commands;

import de.tammo.master.Master;
import de.tammo.master.command.ICommand;
import de.tammo.master.logging.Logger;

@ICommand.CommandInfo(name = "help", aliases = {"a"})
public class HelpCommand implements ICommand{

    public void execute(final String[] args){
        Logger.log("<---Help--->");
        Logger.log("");
        for(final ICommand command : Master.getCommandhandler().getCommands()){
            if(command.getClass().getAnnotation(CommandInfo.class).name().equals("help")){
                continue;
            }
            Logger.log(command.getClass().getAnnotation(CommandInfo.class).name().substring(0, 1).toUpperCase() + command.getClass().getAnnotation(CommandInfo.class).name().substring(1).toLowerCase());
        }
    }

    public void printSyntax(){}
}
