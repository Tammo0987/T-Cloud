package de.tammo.master.command.commands;

import de.tammo.master.Master;
import de.tammo.master.command.ICommand;
import de.tammo.master.core.Wrapper;
import de.tammo.master.logging.LogLevel;
import de.tammo.master.logging.Logger;

@ICommand.CommandInfo(name = "wrapper", aliases = {"w"})
public class WrapperCommand implements ICommand{

    public void execute(final String[] args){
        if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                Logger.log("<--- Wrapper -->");
                Logger.log("");
                for(final Wrapper wrapper : Master.getWrapperHandler().getWrappers()){
                    Logger.log("Wrapper-" + wrapper.getId() + " on Host " + wrapper.getHost() + " on Port " + wrapper.getPort() + " is" + (wrapper.getClient().isConnected() ? "" : " not") + " connected!");
                }
            }else{
                this.printSyntax();
            }
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("add")){
                final int id = Master.getWrapperHandler().nextID();
                Master.getWrapperHandler().addWrapper(new Wrapper(id, args[1], 7801));
                Logger.log("Added Wrapper-" + id + " on Host " + args[1] + " on Port 7801");
            }else if(args[0].equalsIgnoreCase("remove")){
                try {
                    int id = Integer.parseInt(args[1]);
                    final Wrapper wrapper = Master.getWrapperHandler().getWrapperById(id);
                    if(wrapper == null){
                        Logger.log("Wrapper-" + id + " doesnt exist!", LogLevel.ERROR);
                        return;
                    }
                    Master.getWrapperHandler().removeWrapper(wrapper);
                    Logger.log("Removed Wrapper-" + id);
                } catch (NumberFormatException e) {
                    Logger.log("This is not a valid Number", LogLevel.WARNING);
                }
            }else{
                this.printSyntax();
            }
        }else if(args.length == 3){
            if(args[0].equalsIgnoreCase("add")){
                try {
                    int port = Integer.parseInt(args[2]);
                    final int id = Master.getWrapperHandler().nextID();
                    Master.getWrapperHandler().addWrapper(new Wrapper(id, args[1], port));
                    Logger.log("Added Wrapper-" + id + " on Host " + args[1] + " on Port " + port);
                } catch (NumberFormatException e) {
                    Logger.log("This is not a valid Number", LogLevel.WARNING);
                }
            }else{
                this.printSyntax();
            }
        }else{
            this.printSyntax();
        }
    }

    public void printSyntax(){
        Logger.log("wrapper list", LogLevel.WARNING);
        Logger.log("wrapper add <host>", LogLevel.WARNING);
        Logger.log("wrapper add <host> <port>", LogLevel.WARNING);
        Logger.log("wrapper remove <id>", LogLevel.WARNING);
    }
}
