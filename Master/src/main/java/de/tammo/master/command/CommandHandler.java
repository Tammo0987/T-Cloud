package de.tammo.master.command;

import com.google.common.collect.Lists;
import com.google.common.reflect.ClassPath;
import de.tammo.master.logging.*;
import lombok.Getter;

import java.util.*;

public class CommandHandler {

    /**
     * List of all commands
     */
    @Getter
    private final ArrayList<ICommand> commands = Lists.newArrayList();

    /**
     * Constructor to load all commands
     */
    public CommandHandler() {
        try {
            for(final ClassPath.ClassInfo classInfo : ClassPath.from(this.getClass().getClassLoader()).getTopLevelClasses("de.tammo.master.command.commands")){
                final Class command = Class.forName(classInfo.getName());
                this.add((ICommand)command.newInstance());
            }
        } catch (Exception e) {
            Logger.log("Error while loading commands!", LogLevel.CRITICAL);
            Logger.log(e.getMessage());
        }
    }

    /**
     * Get the right command and execute
     * @param exec String to execute
     */
    public void executeCommand(final String exec){
        for(final ICommand command : this.commands){
            if(command.getClass().isAnnotationPresent(ICommand.CommandInfo.class)){
                final ICommand.CommandInfo info = command.getClass().getAnnotation(ICommand.CommandInfo.class);

                final ArrayList<String> trigger = new ArrayList<>();
                trigger.add(info.name());
                Collections.addAll(trigger, info.aliases());

                final String[] arguments = exec.split(" ");
                for(String s : trigger){
                    if(arguments[0].equalsIgnoreCase(s)){
                        final String[] args = new String[arguments.length - 1];
                        System.arraycopy(arguments, 1, args, 0, args.length);
                        command.execute(args);
                        return;
                    }
                }
            }
        }
        Logger.log("Command not found!");
        Logger.log("Try command help");
    }

    /**
     * Added a Command to the List
     * @param command Command
     */
    private void add(final ICommand command){
        this.commands.add(command);
    }

}
