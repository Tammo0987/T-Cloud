package de.tammo.master.command.commands;

import de.tammo.master.Master;
import de.tammo.master.command.ICommand;
import de.tammo.master.logging.Logger;

@ICommand.CommandInfo(name = "stop", aliases = {"s", "end", "shutdown"})
public class StopCommand implements ICommand{

    public void execute(String[] args) {
        Logger.log("Shutdown master...");
        Master.Shutdown();
    }

    public void printSyntax(){}
}
