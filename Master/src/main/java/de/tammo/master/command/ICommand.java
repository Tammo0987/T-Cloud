package de.tammo.master.command;

import java.lang.annotation.*;

public interface ICommand {

    /**
     * Execute the command
     * @param args Arguments
     */
    void execute(final String[] args);

    /**
     * Prints the syntax
     */
    void printSyntax();

    /**
     * Annotation for command info
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @interface CommandInfo{
        String name();
        String[] aliases();
    }
}
