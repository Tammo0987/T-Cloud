package de.tammo.master.command.commands;

import de.tammo.master.Master;
import de.tammo.master.command.ICommand;
import de.tammo.master.logging.*;
import de.tammo.master.security.login.User;

@ICommand.CommandInfo(name = "user", aliases = {"u"})
public class UserCommand implements ICommand{

    public void execute(String[] args) {
        if(args.length < 1){
            this.printSyntax();
            return;
        }
        switch (args[0].toLowerCase()){
            case "list":
                for(User user : Master.getUserhandler().getUsers()){
                    Logger.log(user.getName());
                }
                break;
            case "add":
                if(args.length == 3){
                    final String name = args[1];
                    final String password = args[2];
                    if(name.length() > 4){
                        if(password.length() >= 6){
                            if(Master.getUserhandler().userExists(name) == null){
                                Master.getUserhandler().addUser(name, password);
                                Logger.log("Created user " + name + " successfully!");
                                break;
                            }else{
                                Logger.log("The user exists already!", LogLevel.WARNING);
                                return;
                            }
                        }else{
                            Logger.log("Password must be longer!", LogLevel.WARNING);
                            return;
                        }
                    }else{
                        Logger.log("Name must be longer!", LogLevel.WARNING);
                        return;
                    }
                }else{
                    Logger.log("Wrong length of arguments!", LogLevel.WARNING);
                    return;
                }
            case "remove":
                if(args.length == 2){
                    final String name = args[1];
                    if(Master.getUserhandler().userExists(name) != null){
                        if(Master.getUserhandler().getUsers().size() > 1){
                            Master.getUserhandler().removeUser(name);
                            Logger.log("You removed the user " + name + " successfully!");
                        }else{
                            Logger.log("You can't remove the last user!", LogLevel.WARNING);
                            return;
                        }
                    }else{
                        Logger.log("This user dont exist!", LogLevel.WARNING);
                        return;
                    }
                }else{
                    Logger.log("Wrong length of arguments!", LogLevel.WARNING);
                    return;
                }
                break;
                default:
                    this.printSyntax();
                    break;
        }
    }

    public void printSyntax(){
        Logger.log("user list", LogLevel.WARNING);
        Logger.log("user add <name> <password>", LogLevel.WARNING);
        Logger.log("user remove <name>", LogLevel.WARNING);
    }
}
