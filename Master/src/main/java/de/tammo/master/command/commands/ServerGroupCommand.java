package de.tammo.master.command.commands;

import de.tammo.master.Master;
import de.tammo.master.command.ICommand;
import de.tammo.master.core.group.*;
import de.tammo.master.logging.*;

@ICommand.CommandInfo(name = "servergroup", aliases = {"sg", "serverg", "sgroup"})
public class ServerGroupCommand implements ICommand{

    public void execute(final String[] args){
        if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                if(!Master.getServerCore().getGroups().isEmpty()){
                    Logger.log("<--- Servergroups --->");
                    Logger.log("");
                    for(final ServerGroup group : Master.getServerCore().getGroups()){
                        Logger.log("Name: " + group.getName() + ", Startport: " + group.getStartPort() + ", Type: " + group.getType().toString().substring(0, 1).toUpperCase() + group.getType().toString().substring(1).toLowerCase());
                    }
                }else{
                    Logger.log("There are 0 servergroups!", LogLevel.WARNING);
                }
            }else{
                this.printSyntax();
            }
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("delete")){
                final ServerGroup group = Master.getServerCore().getGroupByName(args[1]);
                if(group == null){
                    Logger.log("This servergroup does not exists!", LogLevel.WARNING);
                    return;
                }
                Master.getServerCore().removeGroup(group);
                Logger.log("Deleted servegroup " + group.getName());
            }else{
                this.printSyntax();
            }
        }else if(args.length == 7){
            if(args[0].equalsIgnoreCase("add")){
                try {
                    final String name = args[1];
                    final int port = Integer.parseInt(args[2]);
                    final int maxServer = Integer.parseInt(args[3]);
                    final int minRam = Integer.parseInt(args[4]);
                    final int maxRam = Integer.parseInt(args[5]);
                    final ServerType type = ServerType.valueOf(args[6].toUpperCase());
                    Master.getServerCore().addGroup(new ServerGroup(name, port, maxServer, minRam, maxRam, type));
                    Logger.log("Created the servergroup " + name);
                } catch (NumberFormatException e) {
                    Logger.log("This ist not a valid number!", LogLevel.WARNING);
                }
            }else{
                this.printSyntax();
            }
        }else{
            this.printSyntax();
        }
    }

    public void printSyntax(){
        Logger.log("servergroup list");
        Logger.log("servergroup add <name> <startport> <maxserver> <minram> <maxram> <type>");
        Logger.log("servergroup delete <name>");
    }
}
