package de.tammo.master.core;

import de.tammo.master.network.client.NetworkClient;
import lombok.Getter;
import lombok.Setter;

public class Wrapper {

    /**
     * Id from wrapper
     */
    @Getter
    private final int id;

    /**
     * Host from wrapper to connect
     */
    @Getter
    private final String host;

    /**
     * Port from wrapper to connect
     */
    @Getter
    private final int port;

    /**
     * Networkclient from the wrapper
     */
    @Setter
    @Getter
    private transient NetworkClient client;

    /**
     * Wrapper constructor
     * @param id Id from wrapper
     * @param host Host from wrapper to connect
     * @param port Port from wrapper to connect
     */
    public Wrapper(final int id, final String host, final int port){
        this.id = id;
        this.host = host.replace("localhost", "127.0.0.1");
        this.port = port;
        this.client = new NetworkClient(host, port);
    }
}
