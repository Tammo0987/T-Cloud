package de.tammo.master.core.group;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ServerGroup {

    /**
     * Name from the group
     */
    @Getter
    private final String name;

    /**
     * Startport on which the first server starts
     */
    @Getter
    private final int startPort;

    /**
     * Number of max. server from the group
     */
    @Getter
    private final int maxServer;

    /**
     * Number of ram to use for one Server
     */
    @Getter
    private final int minRam, maxRam;

    /**
     * The type of the server for this group
     */
    @Getter
    private final ServerType type;

}
