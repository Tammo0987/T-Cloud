package de.tammo.master.core.group;

public enum ServerType {

    DYNAMIC,
    LOBBY,
    STATIC

}
