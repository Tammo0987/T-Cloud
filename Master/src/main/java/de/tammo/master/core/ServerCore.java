package de.tammo.master.core;

import de.tammo.master.core.group.ServerGroup;
import lombok.*;

import java.util.ArrayList;

public class ServerCore {

    /**
     * List of all servergroups
     */
    @Setter
    @Getter
    private ArrayList<ServerGroup> groups = new ArrayList<>();

    /**
     * Add a group to the list
     * @param group group to add
     */
    public void addGroup(final ServerGroup group){
        this.getGroups().add(group);
    }

    /**
     * Removes a
     * @param group
     */
    public void removeGroup(final ServerGroup group){
        if(this.getGroups().contains(group)){
            this.getGroups().remove(group);
        }
    }

    /**
     * @param name name from the servergroup
     * @return a servergroup which has the name from value of name
     */
    public final ServerGroup getGroupByName(final String name) {
        for(final ServerGroup group : this.getGroups()){
            if(group.getName().equalsIgnoreCase(name)){
                return group;
            }
        }
        return null;
    }

}
