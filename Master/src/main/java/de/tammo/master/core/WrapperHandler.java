package de.tammo.master.core;

import de.tammo.master.logging.LogLevel;
import de.tammo.master.logging.Logger;
import de.tammo.master.network.packet.Packet;
import de.tammo.master.network.packets.WrapperConnectPacket;
import de.tammo.master.network.packets.WrapperDisconnectPacket;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class WrapperHandler {

    /**
     * List of all wrappers
     */
    @Setter
    @Getter
    private ArrayList<Wrapper> wrappers = new ArrayList<>();

    /**
     * Add wrapper to the list
     * @param wrapper wrapper to add
     */
    public void addWrapper(final Wrapper wrapper){
        this.wrappers.add(wrapper);
    }

    /**
     * Remove wrapper from the list
     * @param wrapper wrapper to remove
     */
    public void removeWrapper(final Wrapper wrapper){
        if(this.wrappers.contains(wrapper)){
            this.wrappers.remove(wrapper);
        }
    }

    /**
     * @return number of connected wrappers
     */
    public final long getConnected() {
        Logger.log(wrappers.get(0).toString());
        return wrappers.stream().filter((wrapper -> wrapper.getClient().isConnected())).count();
    }

    /**
     * @return next available id
     */
    public final int nextID(){
        return this.wrappers.size() + 1;
    }

    //TODO Optimize -> Reentrant Lock and Condition
    public void connectWrapper(final Wrapper wrapper){
        try {
            wrapper.getClient().connect();
            //TODO
            Thread.sleep(1000);
            if(wrapper.getClient().isConnected()){
                Logger.log("Connected to wrapper-" + wrapper.getId());
                this.sendPacket(wrapper, new WrapperConnectPacket());
            }
        } catch (Exception e) {
            Logger.log("Can't connect to wrapper " + wrapper.getId(), LogLevel.WARNING);
        }
    }

    public void sendPacket(final Wrapper wrapper, final Packet packet){
        wrapper.getClient().sendPacket(packet);
    }

    //TODO sleep?
    public void disconnectWrapper(final Wrapper wrapper){
        this.sendPacket(wrapper, new WrapperDisconnectPacket());
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            Logger.log("Interrupted", LogLevel.DEBUG);
        }
        wrapper.getClient().disconnect();
        Logger.log("Wrapper-" + wrapper.getId() + " disconnected...");
    }

    /**
     * @param host hostname
     * @return wrapper by hostname
     */
    public final Wrapper getWrapperByHost(final String host){
        for(final Wrapper wrapper : this.wrappers){
            if(wrapper.getHost().equalsIgnoreCase(host)){
                return wrapper;
            }
        }
        return null;
    }

    /**
     * @param id wrapper-id
     * @return wrapper by id
     */
    public final Wrapper getWrapperById(final int id){
        for(final Wrapper wrapper : this.wrappers){
            if(wrapper.getId() == id){
                return wrapper;
            }
        }
        return null;
    }

}
