package de.tammo.master.logging.file;

import de.tammo.master.logging.Logger;
import lombok.Cleanup;

import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.*;

public class FileLogger {

    /**
     * DateFormat for the names of files
     */
    private static final SimpleDateFormat FILE_FORMAT = new SimpleDateFormat("yyyy//MM//dd");

    /**
     * Log an object to file
     * @param toLog Object to log
     */
    public static void log(final Object toLog){
        final File file = new File("Log//Master//" + FILE_FORMAT.format(new Date()) + ".log");
        if(!file.getParentFile().exists()){
            try {
                Files.createDirectories(file.getParentFile().toPath());
            } catch (IOException e) {
                Logger.logException("Cant create file!", e);
            }
        }

        if(!file.exists()){
            try {
                Files.createFile(file.toPath());
            } catch (IOException e) {
                Logger.logException("Cant create file!", e);
            }
        }
        logToFile(file, toLog);
    }

    /**
     * Log an error
     * @param toLog Error to log
     */
    public static void errorLog(final Object toLog){
        final File file = new File("Log//Master//" + FILE_FORMAT.format(new Date()) + ".error");
        if(!file.getParentFile().exists()){
            try {
                Files.createDirectories(file.getParentFile().toPath());
            } catch (IOException e) {
                Logger.logException("Cant create file!", e);
            }
        }

        if(!file.exists()){
            try {
                Files.createFile(file.toPath());
            } catch (IOException e) {
                Logger.logException("Cant create file!", e);
            }
        }

        logToFile(file, toLog);
    }

    //TODO Append (Printwriter?)
    /**
     * Log an object to file
     * @param file Logfile
     * @param object Object to log
     */
    private static void logToFile(final File file, final Object object) {
        try {
            final ArrayList<String> list = (ArrayList<String>) Files.readAllLines(file.toPath());
            @Cleanup
            final BufferedWriter bw = Files.newBufferedWriter(file.toPath());
            for(final String s : list){
                bw.write(s);
                bw.newLine();
            }
            bw.write(object.toString());
            bw.flush();
        } catch (IOException e) {
            Logger.logException("Cant write to file!", e);
        }
    }
}
