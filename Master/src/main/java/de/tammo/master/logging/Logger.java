package de.tammo.master.logging;

import de.tammo.master.logging.file.FileLogger;

import java.text.SimpleDateFormat;
import java.util.Date;

import static de.tammo.master.logging.LogLevel.ERROR;

public class Logger {

    /**
     * Prefix for the console
     */
    private final static String PREFIX = " [T-Cloud] -> ";

    /**
     * Dateformat for the timestemp
     */
    private final static SimpleDateFormat TIMESTEMP = new SimpleDateFormat("HH:mm:ss");

    /**
     * Log a message in the console
     * @param object Object to log
     * @param logLevel Level to log
     */
    public static void log(final Object object, final LogLevel logLevel){
        final String time = TIMESTEMP.format(new Date());
        switch (logLevel){
            case DEBUG:
                System.out.println(time + PREFIX + "[DEBUG] " +  object.toString());
                FileLogger.log(time + " [DEBUG] " + object.toString());
                break;
            case INFORMATION:
                System.out.println(time + PREFIX + object.toString());
                FileLogger.log(time + " " + object.toString());
                break;
            case WARNING:
                System.out.println(time + PREFIX + "[WARNING] " +  object.toString());
                FileLogger.log(time + " [WARNING] " + object.toString());
                break;
            case ERROR:
                System.out.println(time + PREFIX + "[ERROR] " +  object.toString());
                FileLogger.errorLog(time + " [ERROR] " + object.toString());
                break;
            case CRITICAL:
                System.out.println(time + PREFIX + "[CRITICAL] " +  object.toString());
                FileLogger.errorLog(time + " [CRITICAL] " + object.toString());
        }
    }

    /**
     * Do the same as above, but the level is automatically information
     * @param object Object to log
     */
    public static void log(final Object object){
        log(object, LogLevel.INFORMATION);
    }

    /**
     * Log an exception
     * @param error Error to log
     * @param exception Exception to log
     */
    public static void logException(final Object error, final Exception exception){
        log(error.toString(), ERROR);
        log(exception.getCause() != null ? exception.getCause().getMessage() : "Exception is null!", ERROR);
    }
}
