package de.tammo.master.logging;

public enum LogLevel {

    /**
     * Level to Log
     */
    DEBUG,
    INFORMATION,
    WARNING,
    ERROR,
    CRITICAL
}
