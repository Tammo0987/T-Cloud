package de.tammo.master.security;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

public class AdressHandler {

    /**
     * List from all adresses
     */
    @Setter
    @Getter
    private ArrayList<String> addresses = new ArrayList<>();

    /**
     * Add a adress to the list
     * @param adress Adress to add
     */
    public void addAdress(final String adress){
        this.addresses.add(adress);
    }

    /**
     * @param adress to check
     * @return if adress is valid
     */
    public boolean isValidAdress(final String adress){
        return this.addresses.contains(adress);
    }

}
