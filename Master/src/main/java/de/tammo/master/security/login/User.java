package de.tammo.master.security.login;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class User {

    /**
     * Name of the user
     */
    @Getter
    private final String name;

    /**
     * Password of the user
     */
    @Getter
    private final String password;

    /**
     * Salt for the userpassword
     */
    @Getter
    private final byte[] salt;

}
