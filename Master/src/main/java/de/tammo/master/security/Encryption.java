package de.tammo.master.security;

import de.tammo.master.logging.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

public class Encryption {

    /**
     * @param password Password to hash
     * @param salt Salt for hashing
     * @return Password as hash
     */
    public static String hash(final String password, final byte[] salt){
        try {
            final MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(salt);

            final byte[] bytes = md.digest(password.getBytes());
            final StringBuilder sb = new StringBuilder();

            for(byte b : bytes){
                sb.append(Integer.toString((b & 0xFF) + 0x100, 16).substring(1));
            }
            return sb.toString();

        } catch (NoSuchAlgorithmException e) {
            Logger.logException("Password encoding not available!", e);
        }
        return "";
    }

    /**
     * @param password Password to verify
     * @param hash Old hash
     * @return Password is valid for the hash
     */
    public static boolean verify(final String password, final String hash, final byte[] salt){
        return hash(password, salt).equals(hash);
    }

    /**
     * @return Random salt
     * @throws NoSuchAlgorithmException exception
     * @throws NoSuchProviderException exception
     */
    public static byte[] getSalt() throws NoSuchAlgorithmException, NoSuchProviderException{
        final SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "SUN");
        final byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }
}
