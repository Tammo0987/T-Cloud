package de.tammo.master.security.login;

import de.tammo.master.logging.LogLevel;
import de.tammo.master.logging.Logger;
import de.tammo.master.logging.file.FileLogger;
import de.tammo.master.security.Encryption;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Scanner;

public class UserHandler {

    /**
     * List of all user
     */
    @Setter
    @Getter
    private ArrayList<User> users = new ArrayList<>();

    /**
     * Add a user to the list
     * @param name Name of the user
     * @param password Password from the user
     */
    public void addUser(final String name, final String password){
        try {
            final byte[] salt = Encryption.getSalt();
            final User user = new User(name, Encryption.hash(password, salt), salt);
            this.users.add(user);
        } catch (Exception e) {
            Logger.logException("Could not add user", e);
        }
    }

    /**
     * Remove the user by name
     * @param name username
     */
    public void removeUser(final String name){
        this.users.remove(this.userExists(name));
    }

    /**
     * Wait for the first user (name)
     * @param scanner Scanner to read
     */
    public void createFirstUserName(final Scanner scanner){
        Logger.log("Please type in the name of the user:");
        String user = scanner.nextLine();
        while (user.length() < 4){
            Logger.log("Your name must be longer!", LogLevel.WARNING);
            Logger.log("Please type in the name of the user:");
            user = scanner.nextLine();
        }
        this.createPasswordForFirstUser(scanner, user);
    }

    /**
     * Waiting for create the password from first user
     * @param scanner Scanner to read
     * @param name Name of the first user
     */
    private void createPasswordForFirstUser(final Scanner scanner, final String name){
        Logger.log("Please type in your password:");
        String password = scanner.nextLine();
        while (password.length() < 6){
            Logger.log("Your password must be longer!", LogLevel.WARNING);
            Logger.log("Please type in your password:");
            password = scanner.nextLine();
        }
        try {
            final byte[] salt = Encryption.getSalt();
            final User user = new User(name, Encryption.hash(password, salt), salt);
            this.users.add(user);
            Logger.log("Created user " + user.getName() + " and logged in!");
        } catch (Exception e) {
            Logger.logException("Could not create a salt!", e);
            System.exit(1);
        }
    }

    /**
     * Login to a User
     * @param scanner Scanner to read
     */
    public void login(final Scanner scanner){
        Logger.log("Login user:");
        String username = scanner.nextLine();
        while (this.userExists(username) == null){
            Logger.log("User not exists!", LogLevel.WARNING);
            Logger.log("Login user:");
            username = scanner.nextLine();
        }
        final User user = this.userExists(username);
        Logger.log("Password:");
        String password = scanner.nextLine();
        while (!Encryption.verify(password, user.getPassword(), user.getSalt())){
            Logger.log("Password is false!", LogLevel.WARNING);
            Logger.log("Password:");
            password = scanner.nextLine();
        }
        Logger.log("You are now logged in!");
        FileLogger.log(user.getName() + " logged in!");
    }

    /**
     * @param name Name of the User
     * @return Checks that User not null
     */
    public User userExists(final String name){
        for(final User user : this.users){
            if(user.getName().equalsIgnoreCase(name)){
                return user;
            }
        }
        return null;
    }

}
