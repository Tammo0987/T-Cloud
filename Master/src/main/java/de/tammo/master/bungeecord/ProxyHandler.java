package de.tammo.master.bungeecord;

import de.tammo.master.logging.Logger;
import de.tammo.master.utils.Utils;
import lombok.Getter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class ProxyHandler {

    /**
     * Waterfall.jar as file
     */
    private File waterfall = new File("Master//Proxy//Waterfall.jar");

    /**
     * Waterfall process
     */
    private Process process;

    /**
     * Waterfall console output
     */
    @Getter
    private BufferedReader output;

    /**
     * Waterfall console inputstream to dispatch commands
     */
    @Getter
    private BufferedWriter input;

    /**
     * ProxyHandler constructor
     */
    public ProxyHandler(){
        if(!this.waterfall.exists()){
            try {
                if(!this.waterfall.getParentFile().exists()){
                    Files.createDirectories(this.waterfall.getParentFile().toPath());
                }
                Utils.download("https://ci.destroystokyo.com/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar", "Master//Proxy//Waterfall.jar");
            } catch (IOException e) {
                Logger.logException("Failed to download waterfall.jar", e);
                e.printStackTrace();
            }
        }
    }

    /**
     * Start waterfall
     */
    public void startWaterfall(){
        new Thread(() -> {
            final ProcessBuilder builder = new ProcessBuilder("java", "-jar", "Waterfall.jar").directory(this.waterfall.getParentFile());

            try {
                this.process = builder.start();

                this.output = new BufferedReader(new InputStreamReader(this.process.getInputStream(), StandardCharsets.UTF_8));
                this.input = new BufferedWriter(new OutputStreamWriter(this.process.getOutputStream(), StandardCharsets.UTF_8));

            } catch (IOException e) {
                Logger.logException("Cant start Waterfall.jar", e);
            }

        }, "waterfall").start();
    }

    /**
     * Execute a command to waterfall
     * @param command comman to dispatch
     * @throws IOException exception
     */
    public void dispatch(final String command) throws IOException{
        if(this.process == null){
            Logger.log("Cant dispatch waterfall command");
            return;
        }

        this.input.write(command);
        this.input.newLine();
        this.input.flush();

        Logger.log("Dispatch Command " + command);
    }

    /**
     * Stops the waterfall proxy
     */
    public void stopWaterfall(){
        this.process.destroy();
        try {
            this.output.close();
            this.input.close();
        } catch (IOException e) {
            Logger.logException("Cant stop waterfall!", e);
        }
        Logger.log("Waterfall stopped");
    }
}
