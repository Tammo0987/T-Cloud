package de.tammo.master;

import de.tammo.master.bungeecord.ProxyHandler;
import de.tammo.master.command.CommandHandler;
import de.tammo.master.core.ServerCore;
import de.tammo.master.core.Wrapper;
import de.tammo.master.core.WrapperHandler;
import de.tammo.master.file.Config;
import de.tammo.master.file.FileHandler;
import de.tammo.master.file.GSONFile;
import de.tammo.master.logging.Logger;
import de.tammo.master.network.packet.PacketRegistry;
import de.tammo.master.network.packets.ErrorPacket;
import de.tammo.master.network.packets.SuccessPacket;
import de.tammo.master.network.packets.WrapperConnectPacket;
import de.tammo.master.network.packets.WrapperDisconnectPacket;
import de.tammo.master.network.server.NetworkServer;
import de.tammo.master.security.AdressHandler;
import de.tammo.master.security.login.UserHandler;
import de.tammo.master.utils.Utils;
import lombok.Getter;

import java.io.IOException;
import java.util.Scanner;

import static de.tammo.master.logging.Logger.log;
import static de.tammo.master.logging.Logger.logException;

public class Master {

    /**
     * Instance of CommandHandler
     */
    @Getter
    private static CommandHandler commandhandler;

    /**
     * Instance of FileHandler
     */
    private static FileHandler filehandler;

    /**
     * Instance of UserHandler
     */
    @Getter
    private static UserHandler userhandler;

    /**
     * Instance of AdressHandler
     */
    @Getter
    private static AdressHandler adresshandler;

    /**
     * Instance of NetworkServer
     */
    @Getter
    private static NetworkServer server;

    /**
     * Instance of WrapperHandler
     */
    @Getter
    private static WrapperHandler wrapperHandler;

    /**
     * Instance of ProxyHandler
     */
    @Getter
    private static ProxyHandler proxyHandler;

    /**
     * Instance of ServerCore
     */
    @Getter
    private static ServerCore serverCore;

    /**
     * Master is runnig
     */
    @Getter
    private static boolean running = false;

    /**
     * Master constructor
     * @throws InterruptedException exception
     */
    private Master() throws InterruptedException{
        running = true;
        log("Starting master...");

        this.sleep(100);

        log(" _____     ____   _      _____   _   _   _____  ");
        log("|__ __|   |  __| | |    | ___ | | | | | |  __ | ");
        log("  | |     | |    | |    ||   || | | | | | |  | |");
        log("  | |     | |__  | |__  ||___|| | |_| | | |__| |");
        log("  |_|     |____| |____| |_____| |_____| |_____| ");

        log(""); this.sleep(100);
        log("© Copyright by Tammo0987 2017 - 2018 | Version 1.0-SNAPSHOT");
        this.sleep(100);
        log(""); this.sleep(100);

        this.setupInstances();

        final Scanner scanner = new Scanner(System.in);

        this.setupServer();

        Runtime.getRuntime().addShutdownHook(new Thread(Master::Shutdown));

        this.setupCloudSystem(scanner);

        String line;
        while ((line = scanner.nextLine()) != null){
            if(Utils.isStringValid(line)){
                commandhandler.executeCommand(line);
            }
        }

        scanner.close();
    }

    /**
     * main method
     * @param args java arguments
     */
    public static void main(final String[] args) {
        try {
            new Master();
        } catch (InterruptedException e) {
            logException("Starting was interrupted!", e);
        }
    }

    /**
     * Setup Cloud System
     *
     * @param scanner scanner to read
     */
    private void setupCloudSystem(final Scanner scanner){
        if (userhandler.getUsers().isEmpty()) {
            Logger.log("Welcome to the Setup of T-Cloud!");
            Logger.log("Creating the first User");
            userhandler.createFirstUserName(scanner);
        } else {
            userhandler.login(scanner);
        }

        server.bind();

        if (wrapperHandler.getWrappers().isEmpty()) {
            Logger.log("Please type in the host of Wrapper-1:");
            final String host = scanner.nextLine();
            final Wrapper wrapper = new Wrapper(wrapperHandler.nextID(), host, 7801);
            wrapperHandler.addWrapper(wrapper);
            Logger.log("Added Wrapper-1 on " + host + " on Port 7801");
            Logger.log("Connecting to Wrapper-1...");
            Logger.log("Waiting for Wrapper-1...");
            wrapperHandler.connectWrapper(wrapper);
            while (!wrapper.getClient().isConnected()) {
                System.out.flush();
            }
        }else{
            if(wrapperHandler.getConnected() == 0){
                Logger.log("Connect to Wrappers...");
                for(final Wrapper wrapper : wrapperHandler.getWrappers()){
                    wrapperHandler.connectWrapper(wrapper);
                    wrapperHandler.sendPacket(wrapper, null);
                }
            }
        }
    }

    /**
     * Stop the master
     */
    public static void Shutdown(){
        if(!running){
            return;
        }
        running = false;
        for(final GSONFile file : filehandler.getFiles()){
            try {
                file.save();
            } catch (IOException e) {
                logException("Could not save File: " + file.toString(), e);
            }
        }

        for(final Wrapper wrapper : wrapperHandler.getWrappers()){
            if(wrapper.getClient().isConnected()){
                wrapperHandler.disconnectWrapper(wrapper);
            }
        }

        proxyHandler.stopWaterfall();

        server.close();
        Thread.currentThread().interrupt();
        System.exit(1);
    }

    /**
     * Method to wait
     *
     * @param ms time to wait in ms
     * @throws InterruptedException exception
     */
    private void sleep(final long ms) throws InterruptedException {
        Thread.sleep(ms);
    }

    /**
     * Creates all important instances
     */
    private void setupInstances() {
        commandhandler = new CommandHandler();
        userhandler = new UserHandler();
        adresshandler = new AdressHandler();
        wrapperHandler = new WrapperHandler();
        serverCore = new ServerCore();
        proxyHandler = new ProxyHandler();
        filehandler = new FileHandler();
    }

    /**
     * Setup and start the server
     */
    private void setupServer() {
        PacketRegistry.addPacket(new SuccessPacket());
        PacketRegistry.addPacket(new ErrorPacket());
        PacketRegistry.addPacket(new WrapperConnectPacket());
        PacketRegistry.addPacket(new WrapperDisconnectPacket());
        server = new NetworkServer(Config.port);
        proxyHandler.startWaterfall();
    }
}
