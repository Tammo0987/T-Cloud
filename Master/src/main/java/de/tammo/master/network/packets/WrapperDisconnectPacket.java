package de.tammo.master.network.packets;

import de.tammo.master.Master;
import de.tammo.master.core.Wrapper;
import de.tammo.master.network.packet.Packet;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;

public class WrapperDisconnectPacket extends Packet {

    /**
     * Packet Constructor
     */
    public WrapperDisconnectPacket(){
        super((byte) 0x03);
    }

    public Packet handle(final String host){
        final Wrapper wrapper = Master.getWrapperHandler().getWrapperByHost(host);
        if(wrapper == null){
            return null;
        }
        if(Master.isRunning()){
            Master.getWrapperHandler().disconnectWrapper(wrapper);
        }
        return null;
    }

    public void read(final ByteBufInputStream byteBuf) throws IOException{}

    public void write(final ByteBufOutputStream byteBuf) throws IOException{}
}
