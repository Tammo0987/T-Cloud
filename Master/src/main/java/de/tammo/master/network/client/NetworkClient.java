package de.tammo.master.network.client;

import de.tammo.master.logging.Logger;
import de.tammo.master.network.packet.Packet;
import de.tammo.master.network.packet.PacketDecoder;
import de.tammo.master.network.packet.PacketEncoder;
import de.tammo.master.network.packet.PacketHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Getter;

public class NetworkClient {

    /**
     * Hostadress to connect
     */
    private final String host;

    /**
     * Port to connect
     */
    private final int port;

    /**
     * Epoll is available
     */
    private final boolean EPOLL = Epoll.isAvailable();

    /**
     * Worker EventLoopGroup
     */
    private EventLoopGroup workerGroup;

    /**
     * PacketHandler to handle and send packets
     */
    private PacketHandler packetHandler = new PacketHandler();

    /**
     * Is connected
     */
    @Getter
    private boolean connected;


    /**
     * NetworkClient constructor
     * @param host Hostadress
     * @param port Port
     */
    public NetworkClient(final String host, final int port){
        this.host = host;
        this.port = port;
    }

    /**
     * Connect to the server
     * @return NetworkClient instance
     */
    public final NetworkClient connect() {
        new Thread(() -> {
            this.workerGroup = this.EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();

            try {
                final Bootstrap bootstrap = new Bootstrap()
                        .group(workerGroup)
                        .channel(this.EPOLL ? EpollSocketChannel.class : NioSocketChannel.class)
                        .option(ChannelOption.SO_KEEPALIVE, true)
                        .handler(new ChannelInitializer<SocketChannel>() {

                            @Override
                            protected void initChannel(final SocketChannel channel) {
                                channel.pipeline().addLast(new PacketDecoder()).addLast(new PacketEncoder()).addLast(packetHandler);
                            }

                        });

                final ChannelFuture future = bootstrap.connect(this.host, this.port).sync();

                this.connected = true;

                future.channel().closeFuture().syncUninterruptibly();
            } catch (Exception e) {
                Logger.logException("Cant connect to " + this.host + " on Port " + this.port, e);
                Logger.log("Please start wrapper...");
                this.workerGroup.shutdownGracefully();
                this.connected = false;
            }finally {
                this.workerGroup.shutdownGracefully();
                this.connected = false;
            }

        }).start();
        return this;
    }

    /**
     * Send a packet
     * @param packet Packet
     */
    public void sendPacket(final Packet packet){
        this.packetHandler.sendPacket(packet);
    }

    /**
     * Disconnect from the server
     */
    public void disconnect(){
        this.workerGroup.shutdownGracefully();
    }

}
