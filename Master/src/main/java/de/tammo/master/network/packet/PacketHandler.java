package de.tammo.master.network.packet;

import de.tammo.master.logging.*;
import io.netty.channel.*;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;

@ChannelHandler.Sharable
public class PacketHandler extends SimpleChannelInboundHandler<Packet>{

    /**
     * Channel Connection
     */
    private Channel channel;

    @Setter
    private String host;

    /**
     * Send Packet by init Channel Connection
     */
    private ArrayList<Packet> sendInit = new ArrayList<>();

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception{
        this.channel = ctx.channel();

        if(this.channel != null){
            for(final Packet packet : this.sendInit){
                this.sendPacket(packet);
            }
            this.sendInit.clear();
        }

    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Packet packet) throws Exception{
        if(this.channel == null){
            return;
        }

        final Packet response = packet.handle(this.host);

        if(response != null){
            this.channel.writeAndFlush(response);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception{}

    /**
     * Send a Packet
     * @param packet Packet
     */
    public void sendPacket(final Packet packet){
        if(this.channel == null){
            this.sendInit.add(packet);
            return;
        }
        if(packet == null)
            return;
        this.channel.writeAndFlush(packet);
    }
}