package de.tammo.master.network.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;
import java.net.SocketAddress;

public abstract class Packet {

    /**
     * Packet ID to identifier
     */
    private final byte id;

    /**
     * Packet Constructor
     * @param id ID
     */
    public Packet(final byte id){
        this.id = id;
    }

    /**
     * @return ID from Packet
     */
    final byte getId(){
        return this.id;
    }

    /**
     * Handle the Packet
     * @return Reponse Packet
     */
    public abstract Packet handle(final String host);

    /**
     * Read Data from Packet
     * @param byteBuf ByteBuf to read
     * @throws IOException Exception
     */
    public abstract void read(final ByteBufInputStream byteBuf) throws IOException;

    /**
     * Write Data to Packet
     * @param byteBuf ByteBuf to write
     * @throws IOException Exception
     */
    public abstract void write(final ByteBufOutputStream byteBuf) throws IOException;

}
