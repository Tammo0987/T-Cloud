package de.tammo.master.network.packets;

import de.tammo.master.Master;
import de.tammo.master.core.Wrapper;
import de.tammo.master.network.packet.Packet;
import io.netty.buffer.*;

import java.io.IOException;

public class WrapperConnectPacket extends Packet{

    /**
     * Packet Constructor
     */
    public WrapperConnectPacket(){
        super((byte) 0x02);
    }

    public Packet handle(final String host){
        final Wrapper wrapper = Master.getWrapperHandler().getWrapperByHost(host);
        if(wrapper == null){
            return new ErrorPacket("Wrapper is null!");
        }
        if(!wrapper.getClient().isConnected()){
            Master.getWrapperHandler().connectWrapper(wrapper);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return wrapper.getClient().isConnected() ? new SuccessPacket() : new ErrorPacket("Wrapper is not connected!");
    }

    public void read(final ByteBufInputStream byteBuf) throws IOException{}

    public void write(final ByteBufOutputStream byteBuf) throws IOException{}
}
