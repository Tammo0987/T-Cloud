package de.tammo.master.network.packet;

import io.netty.buffer.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class PacketDecoder extends ByteToMessageDecoder{

    /**
     * Decodes a Packet
     * @param ctx ChannelHandlerContext
     * @param byteBuf ByteBuf
     * @param list List
     * @throws Exception Exception
     */
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> list) throws Exception{
        final Packet packet = PacketRegistry.getPacketById((byte) byteBuf.readInt());
        if(packet != null){
            packet.read(new ByteBufInputStream(byteBuf));
            list.add(packet);
        }
    }

}