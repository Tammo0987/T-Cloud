package de.tammo.master.network.packets;

import de.tammo.master.logging.*;
import de.tammo.master.network.packet.Packet;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;

public class ErrorPacket extends Packet{

    private String error;

    public ErrorPacket(){
        super((byte) 0x01);
    }

    public ErrorPacket(String error){
        super((byte) 0x01);
        this.error = error;
    }

    public Packet handle(final String host){
        Logger.log(error, LogLevel.ERROR);
        return null;
    }

    public void read(ByteBufInputStream byteBuf) throws IOException{
        this.error = byteBuf.readUTF();
    }

    public void write(ByteBufOutputStream byteBuf) throws IOException{
        byteBuf.writeUTF(error);
    }
}
