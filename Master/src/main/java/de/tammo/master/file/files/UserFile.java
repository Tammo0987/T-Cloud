package de.tammo.master.file.files;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import de.tammo.master.Master;
import de.tammo.master.file.GSONFile;
import de.tammo.master.logging.Logger;
import de.tammo.master.security.login.User;

import java.io.*;
import java.util.ArrayList;

public class UserFile extends GSONFile {

    public UserFile() {
        super("user");
     }

    public void load() throws IOException {
        try {
            final BufferedReader br = this.getBufferedReader();

            Master.getUserhandler().setUsers(new Gson().fromJson(br, new TypeToken<ArrayList<User>>(){}.getType()));
            br.close();
        }catch (NullPointerException e){
            Logger.logException("Could not load User Config", e);
        }
    }

    public void save() throws IOException {
        try {
            final BufferedWriter bw = this.getBufferedWriter();
            final Gson gson = new GsonBuilder().setPrettyPrinting().create();

            bw.write(gson.toJson(Master.getUserhandler().getUsers()));
            bw.close();
        }catch (NullPointerException e){
            Logger.logException("Could not save User Config!", e);
        }
    }
}
