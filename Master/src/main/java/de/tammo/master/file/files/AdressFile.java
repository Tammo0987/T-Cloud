package de.tammo.master.file.files;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import de.tammo.master.Master;
import de.tammo.master.file.GSONFile;
import de.tammo.master.logging.Logger;

import java.io.*;
import java.util.ArrayList;

public class AdressFile extends GSONFile{

    public AdressFile() {
        super("adresses");
    }

    public void load() throws IOException {
        try {
            BufferedReader br = getBufferedReader();

            Master.getAdresshandler().setAddresses(new Gson().fromJson(br, new TypeToken<ArrayList<String>>(){}.getType()));
            br.close();
        }catch (NullPointerException e){
            Logger.logException("Could not load Adresses Config!", e);
        }
    }

    public void save() throws IOException {
        try {
            BufferedWriter bw = getBufferedWriter();
            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            if(Master.getAdresshandler().getAddresses().size() == 0){
                Master.getAdresshandler().addAdress("127.0.0.1");
            }

            bw.write(gson.toJson(Master.getAdresshandler().getAddresses()));
            bw.close();
        }catch (NullPointerException e){
            Logger.logException("Could not save Adresses Config!", e);
        }
    }
}
