package de.tammo.master.file.files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import de.tammo.master.Master;
import de.tammo.master.core.Wrapper;
import de.tammo.master.file.GSONFile;
import de.tammo.master.logging.Logger;
import de.tammo.master.network.client.NetworkClient;
import lombok.Cleanup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WrapperFile extends GSONFile{

    /**
     * GSONFile Constructor
     */
    public WrapperFile(){
        super("wrappers");
    }

    public void load() throws IOException {
        try {
            @Cleanup final BufferedReader br = this.getBufferedReader();
            Master.getWrapperHandler().setWrappers(new Gson().fromJson(br, new TypeToken<ArrayList<Wrapper>>(){}.getType()));
            Master.getWrapperHandler().getWrappers().forEach((wrapper -> wrapper.setClient(new NetworkClient(wrapper.getHost(), wrapper.getPort()))));
        } catch (NullPointerException e) {
            Logger.logException("Could not load Wrapper Config", e);
        }
    }

    public void save() throws IOException{
        try {
            @Cleanup final BufferedWriter bw = this.getBufferedWriter();
            final Gson gson = new GsonBuilder().setPrettyPrinting().create();

            bw.write(gson.toJson(Master.getWrapperHandler().getWrappers()));
        } catch (NullPointerException e) {
            Logger.logException("Could not save wrapper config", e);
        }
    }
}
