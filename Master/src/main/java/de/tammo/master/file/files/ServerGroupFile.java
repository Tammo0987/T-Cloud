package de.tammo.master.file.files;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import de.tammo.master.Master;
import de.tammo.master.core.group.ServerGroup;
import de.tammo.master.file.GSONFile;
import de.tammo.master.logging.Logger;

import java.io.*;
import java.util.ArrayList;

public class ServerGroupFile extends GSONFile{

    /**
     * GSONFile Constructor
     */
    public ServerGroupFile(){
        super("servergroups");
    }

    public void load() throws IOException{
        try {
            final BufferedReader br = this.getBufferedReader();

            Master.getServerCore().setGroups(new Gson().fromJson(br, new TypeToken<ArrayList<ServerGroup>>(){}.getType()));
            br.close();
        } catch (NullPointerException e) {
            Logger.logException("Could not load ServerGroup Config", e);
        }
    }

    public void save() throws IOException {
        try {
            final BufferedWriter bw = this.getBufferedWriter();
            final Gson gson = new GsonBuilder().setPrettyPrinting().create();

            bw.write(gson.toJson(Master.getServerCore().getGroups()));
            bw.close();
        }catch (NullPointerException e){
            Logger.logException("Could not save ServerGroup Config!", e);
        }
    }

}
