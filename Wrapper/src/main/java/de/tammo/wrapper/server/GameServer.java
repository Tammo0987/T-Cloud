package de.tammo.wrapper.server;

import de.tammo.wrapper.logging.Logger;
import de.tammo.wrapper.template.Template;
import de.tammo.wrapper.utils.Utils;
import lombok.Getter;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class GameServer {

    /**
     * Name from server
     */
    @Getter
    private final String name;

    /**
     * Port from server
     */
    @Getter
    private final int port;

    /**
     * Ram usage
     */
    private final int minRam, maxRam;

    /**
     * Template for copy files
     */
    private final Template template;

    /**
     * Process which contains server thread
     */
    private Process process;

    @Getter
    private BufferedReader ouput;

    @Getter
    private BufferedWriter input;

    /**
     * Gameserver constructor
     * @param name name
     * @param port port
     * @param minRam minRam
     * @param maxRam maxRam
     */
    public GameServer(final String name, final int port, final int minRam, final int maxRam, final Template template) {
        this.name = name;
        this.port = port;
        this.minRam = minRam;
        this.maxRam = maxRam;
        this.template = template;
    }

    /**
     * Starts the Server
     */
    public void startServer(){
        new Thread(() -> {
            Logger.log("Gameserver " + this.name + " started on port: " + this.port + "...");

            final File global = new File("Wrapper//Global");
            final File tempDir = new File("Wrapper//temp//" + this.name);

            Utils.copyDir(global, tempDir);
            Utils.copyDir(this.template.getPath(), tempDir);

            final ProcessBuilder builder = new ProcessBuilder("java", "-Xmx" + this.maxRam + "M", "-Xms" + this.minRam + "M", "-jar", "spigot.jar", "-port", String.valueOf(this.port)).directory(tempDir);

            try {
                this.process = builder.start();

                this.ouput = new BufferedReader(new InputStreamReader(this.process.getInputStream(), StandardCharsets.UTF_8));
                this.input = new BufferedWriter(new OutputStreamWriter(this.process.getOutputStream(), StandardCharsets.UTF_8));
            } catch (IOException e) {
                Logger.logException("Error by starting server " + this.name, e);
            }
        }, "Gameserver " + this.name).start();
    }

    /**
     * @return the Output from the Server
     */
    public final BufferedReader getConsoleOutput(){
        return new BufferedReader(new InputStreamReader(this.process.getInputStream(), StandardCharsets.UTF_8));
    }

    public void dispatch(final String command) throws IOException{
        if(this.process == null){
            Logger.log("Cant dispatch command");
            return;
        }
        this.input.write(command);
        this.input.newLine();
        this.input.flush();
    }

    /**
     * Stops the Server
     */
    public void stopServer(){
        this.process.destroy();
        try {
            this.ouput.close();
            this.input.close();
        } catch (IOException e) {
            Logger.logException("Cant close server " + this.name, e);
        }
        Utils.deleteDir(new File("Wrapper//temp//" + this.name));
        Logger.log("Server " + this.name + " stopped");
    }

}
