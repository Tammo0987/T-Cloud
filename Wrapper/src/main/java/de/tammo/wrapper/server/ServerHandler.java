package de.tammo.wrapper.server;

import lombok.Getter;

import java.util.ArrayList;

public class ServerHandler {

    /**
     * List of all gameservers
     */
    @Getter
    private final ArrayList<GameServer> server = new ArrayList<>();

    /**
     * Add server to the list
     * @param server Server to add
     */
    public void addServer(final GameServer server){
        this.server.add(server);
    }

    /**
     * Remove server
     * @param server eerver to remove
     */
    public void removeServer(final GameServer server){
        if(this.server.contains(server)){
            this.server.remove(server);
        }
    }

}
