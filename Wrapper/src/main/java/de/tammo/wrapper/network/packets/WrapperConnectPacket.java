package de.tammo.wrapper.network.packets;

import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.network.packet.Packet;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

public class WrapperConnectPacket extends Packet {

    public WrapperConnectPacket(){
        super((byte) 0x02);
    }

    //TODO Multithreading
    public final Packet handle(final String host) {
        if(!Wrapper.isConnect()){
            Wrapper.getClient().connect();
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Wrapper.isConnect() ? new SuccessPacket() : new ErrorPacket("Can´t connect to Wrapper");
    }

    public void read(final ByteBufInputStream byteBuf) {
    }

    public void write(final ByteBufOutputStream byteBuf) {
    }
}
