package de.tammo.wrapper.network.client;

import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.logging.Logger;
import de.tammo.wrapper.network.packet.Packet;
import de.tammo.wrapper.network.packet.PacketDecoder;
import de.tammo.wrapper.network.packet.PacketEncoder;
import de.tammo.wrapper.network.packet.PacketHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NetworkClient {

    /**
     * Hostadress to connect
     */
    private final String host;

    /**
     * Port to connect
     */
    private final int port;

    /**
     * Epoll is available
     */
    private final boolean EPOLL = Epoll.isAvailable();

    /**
     * Worker EventLoopGroup
     */
    private EventLoopGroup workerGroup;

    /**
     * PacketHandler to handle and send packets
     */
    private PacketHandler packetHandler = new PacketHandler();


    /**
     * NetworkClient constructor
     * @param host Hostadress
     * @param port Port
     */
    public NetworkClient(final String host, final int port){
        this.host = host;
        this.port = port;
    }

    /**
     * Connect to the server
     * @return NetworkClient instance
     */
    public NetworkClient connect(){
        new Thread(this::run).start();
        return this;
    }

    /**
     * Send a packet
     * @param packet Packet
     */
    public void sendPacket(final Packet packet){
        this.packetHandler.sendPacket(packet);
    }

    /**
     * Disconnect from the server
     */
    public void disconnect(){
        this.workerGroup.shutdownGracefully();
        Wrapper.setConnect(false);
        Logger.log("Disconnected from Master...");
    }

    /**
     * Start the server
     */
    private void run(){
        this.workerGroup = this.EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();

        try {
            final Bootstrap bootstrap = new Bootstrap()
                    .group(workerGroup)
                    .channel(this.EPOLL ? EpollSocketChannel.class : NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {

                        protected void initChannel(final SocketChannel channel) throws Exception{

                            channel.pipeline().addLast(new PacketDecoder()).addLast(new PacketEncoder()).addLast(packetHandler);
                            if(channel.remoteAddress() != null){
                                packetHandler.setHost(channel.remoteAddress().getAddress().getHostAddress());
                            }

                        }

                    });

            final ChannelFuture future = bootstrap.connect(this.host, this.port).sync();

            Wrapper.setConnect(true);

            future.channel().closeFuture().sync();
        } catch (Exception e) {
            Logger.logException("Cant connect to " + this.host + " on Port " + this.port, e);
            Wrapper.setConnect(false);
        } finally {
            this.workerGroup.shutdownGracefully();
            Wrapper.setConnect(false);
        }
    }
}
