package de.tammo.wrapper.network.packets;

import de.tammo.wrapper.network.packet.Packet;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

public class SuccessPacket extends Packet {

    public SuccessPacket(){
        super((byte) 0x00);
    }

    public final Packet handle(final String host) {
        return null;
    }

    public void read(ByteBufInputStream byteBuf) {
    }

    public void write(ByteBufOutputStream byteBuf) {
    }
}
