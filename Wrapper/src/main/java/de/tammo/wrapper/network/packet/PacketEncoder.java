package de.tammo.wrapper.network.packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class PacketEncoder extends MessageToByteEncoder<Packet>{

    /**
     * Encodes a packet
     * @param ctx channelhandlercontext
     * @param packet packet to encode
     * @param byteBuf bytebuf
     * @throws Exception exception
     */
    protected void encode(final ChannelHandlerContext ctx, final Packet packet, final ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(packet.getId());
        packet.write(new ByteBufOutputStream(byteBuf));
    }

}
