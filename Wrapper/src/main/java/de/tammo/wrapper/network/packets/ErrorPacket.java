package de.tammo.wrapper.network.packets;

import de.tammo.wrapper.logging.LogLevel;
import de.tammo.wrapper.logging.Logger;
import de.tammo.wrapper.network.packet.Packet;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;

public class ErrorPacket extends Packet {

    private String error;

    public ErrorPacket(){
        super((byte) 0x01);
    }

    public ErrorPacket(String error){
        super((byte) 0x01);
        this.error = error;
    }

    public final Packet handle(final String host) {
        Logger.log(this.error, LogLevel.ERROR);
        return null;
    }

    public void read(final ByteBufInputStream byteBuf) throws IOException {
        this.error = byteBuf.readUTF();
    }

    public void write(final ByteBufOutputStream byteBuf) throws IOException {
        byteBuf.writeUTF(this.error);
    }
}
