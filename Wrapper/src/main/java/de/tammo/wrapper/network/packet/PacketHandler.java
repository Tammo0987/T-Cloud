package de.tammo.wrapper.network.packet;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.Setter;

import java.util.ArrayList;

@ChannelHandler.Sharable
public class PacketHandler extends SimpleChannelInboundHandler<Packet>{

    /**
     * Channel from connection
     */
    private Channel channel;

    /**
     * Host from connection
     */
    @Setter
    private String host;

    /**
     * Send packet by init channel connection
     */
    private ArrayList<Packet> sendInit = new ArrayList<>();

    /**
     * Init channel and send init packets
     *
     * @param ctx channelhandlercontext
     * @throws Exception exception
     */
    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
        this.channel = ctx.channel();

        for(final Packet packet : this.sendInit){
            this.sendPacket(packet);
        }
    }

    /**
     * Read channel to receive packets
     *
     * @param ctx    channelhandlercontext
     * @param packet packet to read
     * @throws Exception exception
     */
    protected void channelRead0(final ChannelHandlerContext ctx, final Packet packet) throws Exception{
        if(this.channel == null){
            return;
        }

        final Packet response = packet.handle(this.host);

        if(response != null){
            this.channel.writeAndFlush(response);
        }
    }

    /**
     * Caught exceptions in connection
     *
     * @param ctx   channelhandlercontext
     * @param cause throwable
     */
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    }

    /**
     * send a packet
     * @param packet packet to send
     */
    public void sendPacket(final Packet packet){
        if(this.channel == null){
            this.sendInit.add(packet);
            return;
        }
        this.channel.writeAndFlush(packet);
    }
}