package de.tammo.wrapper.network.server;

import de.tammo.wrapper.logging.Logger;
import de.tammo.wrapper.network.packet.PacketDecoder;
import de.tammo.wrapper.network.packet.PacketEncoder;
import de.tammo.wrapper.network.packet.PacketHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NetworkServer {

    /**
     * Port to bind
     */
    private final int port;

    /**
     * Epoll available
     */
    private final boolean EPOLL = Epoll.isAvailable();

    /**
     * Boss EventLoopGroup
     */
    private EventLoopGroup bossGroup;

    /**
     * Worker EventLoopGroup
     */
    private EventLoopGroup workerGroup;

    /**
     * Packethandler instance
     */
    private PacketHandler packetHandler = new PacketHandler();

    /**
     * NetworkServer constructor
     * @param port Port
     */
    public NetworkServer(final int port){
        this.port = port;
    }

    /**
     * Bind the server on port
     */
    public NetworkServer bind(){
        new Thread(() -> {
            this.bossGroup = this.EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();
            this.workerGroup = this.EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();

            try {
                final ServerBootstrap bootstrap = new ServerBootstrap()
                        .group(this.bossGroup, this.workerGroup)
                        .channel(this.EPOLL ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
                        .childOption(ChannelOption.SO_KEEPALIVE, true)
                        .childHandler(new ChannelInitializer<SocketChannel>() {

                            @Override
                            protected void initChannel(SocketChannel channel) throws Exception{

                                channel.pipeline().addLast(new PacketEncoder()).addLast(new PacketDecoder()).addLast(packetHandler);
                                if(channel.remoteAddress() != null){
                                    packetHandler.setHost(channel.remoteAddress().getAddress().getHostAddress());
                                }

                            }

                        })
                        .option(ChannelOption.SO_BACKLOG, 128)
                        .childOption(ChannelOption.SO_KEEPALIVE, true);


                final ChannelFuture future = bootstrap.bind(this.port).sync();

                future.channel().closeFuture().sync();
            } catch (Exception e) {
                Logger.logException("Cant bind the server on port " + this.port, e);
            }finally {
                this.bossGroup.shutdownGracefully();
                this.workerGroup.shutdownGracefully();
            }

        }).start();
        return this;
    }

    /**
     * Close the server
     */
    public void close(){
        this.bossGroup.shutdownGracefully();
        this.workerGroup.shutdownGracefully();
        Logger.log("The server was closed...");
    }

}
