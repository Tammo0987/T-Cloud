package de.tammo.wrapper.network.packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class PacketDecoder extends ByteToMessageDecoder{

    /**
     * Decodes a packet
     * @param ctx channelhandlercontext
     * @param byteBuf bytebuf
     * @param list list
     * @throws Exception exception
     */
    protected void decode(final ChannelHandlerContext ctx, final ByteBuf byteBuf, final List<Object> list) throws Exception {
        final Packet packet = PacketRegistry.getPacketById((byte) byteBuf.readInt());
        if(packet != null){
            packet.read(new ByteBufInputStream(byteBuf));
            list.add(packet);
        }
    }

}