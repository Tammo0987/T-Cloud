package de.tammo.wrapper.network.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;

public abstract class Packet {

    /**
     * Packet id to identifier
     */
    private final byte id;

    /**
     * Packet constructor
     * @param id id
     */
    public Packet(final byte id){
        this.id = id;
    }

    /**
     * @return id from packet
     */
    final byte getId(){
        return this.id;
    }

    /**
     * Handling the packet
     * @return Reponse packet
     */
    public abstract Packet handle(final String host);

    /**
     * Read data from packet
     * @param byteBuf ByteBuf to read
     * @throws IOException exception
     */
    public abstract void read(final ByteBufInputStream byteBuf) throws IOException;

    /**
     * Write data to packet
     * @param byteBuf ByteBuf to write
     * @throws IOException exception
     */
    public abstract void write(final ByteBufOutputStream byteBuf) throws IOException;

}
