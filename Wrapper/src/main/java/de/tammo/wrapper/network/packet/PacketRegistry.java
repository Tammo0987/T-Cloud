package de.tammo.wrapper.network.packet;

import java.util.ArrayList;

public class PacketRegistry {

    /**
     * List of all registered packets
     */
    private static ArrayList<Packet> packets = new ArrayList<>();

    /**
     * Add packet to registry
     * @param packet packet
     */
    public static void addPacket(final Packet packet){
        packets.add(packet);
    }

    /**
     * @param id packet id
     * @return Packet instance by id
     */
    static Packet getPacketById(final byte id){
        for(final Packet packet : packets){
            if(packet.getId() == id){
                return packet;
            }
        }
        return null;
    }

    /**
     * Remove packet from registry
     * @param packet Packet
     */
    public static void removePacket(final Packet packet){
        if(packets.contains(packet)){
            packets.remove(packet);
        }
    }

}
