package de.tammo.wrapper.network.packets;

import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.network.packet.Packet;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

public class WrapperDisconnectPacket extends Packet{

    public WrapperDisconnectPacket(){
        super((byte) 0x03);
    }

    public final Packet handle(final String host) {
        if(Wrapper.isRunning()){
            Wrapper.Shutdown();
        }
        return null;
    }

    public void read(final ByteBufInputStream byteBuf) {
    }

    public void write(final ByteBufOutputStream byteBuf) {
    }
}
