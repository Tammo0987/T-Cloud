package de.tammo.wrapper.logging;

import de.tammo.wrapper.logging.file.FileLogger;

import java.text.SimpleDateFormat;
import java.util.Date;

import static de.tammo.wrapper.logging.LogLevel.*;

public class Logger {

    /**
     * Prefix before everey Message in the Console
     */
    private final static String PREFIX = " [Wrapper] -> ";

    /**
     * Dateformat for the time
     */
    private final static SimpleDateFormat timeStemp = new SimpleDateFormat("HH:mm:ss");

    /**
     * Log a Message in the Console
     * @param object Object to log
     * @param logLevel Level to log
     */
    public static void log(final Object object, final LogLevel logLevel){
        final String time = timeStemp.format(new Date());
        switch (logLevel){
            case INFORMATION:
                System.out.println(time + PREFIX + object.toString());
                FileLogger.log(time + " " + object.toString());
                break;
            case WARNING:
                System.out.println(time + PREFIX + "[WARNING] " +  object.toString());
                FileLogger.log(time + " [WARNING] " + object.toString());
                break;
            case ERROR:
                System.out.println(time + PREFIX + "[ERROR] " +  object.toString());
                FileLogger.errorLog(time + " [ERROR] " + object.toString());
                break;
            case CRITICAL:
                System.out.println(time + PREFIX + "[CRITICAL] " +  object.toString());
                FileLogger.errorLog(time + " [CRITICAL] " + object.toString());
        }
    }

    /**
     * Do the same as above, but the level is automatically Information
     * @param object Object to log
     */
    public static void log(final Object object){
        log(object, LogLevel.INFORMATION);
    }

    /**
     * Log a Exception
     * @param error Error to log
     * @param exception Exception to log
     */
    public static void logException(final Object error, final Exception exception){
        log(error.toString(), ERROR);
        log(exception.getMessage(), ERROR);
    }

}
