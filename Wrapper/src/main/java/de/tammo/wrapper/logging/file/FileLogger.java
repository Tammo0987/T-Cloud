package de.tammo.wrapper.logging.file;

import de.tammo.wrapper.logging.Logger;
import lombok.Cleanup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class FileLogger {

    /**
     * Dateformat for the names of the files
     */
    private static final SimpleDateFormat FILE_TIME_FORMAT = new SimpleDateFormat("yyyy//MM//dd");

    /**
     * Log an Object to the file
     * @param toLog Object to log
     */
    public static void log(final Object toLog){
        final File file = new File("Log//Wrapper//" + FILE_TIME_FORMAT.format(new Date()) + ".log");
        if(!file.getParentFile().exists()){
            try {
                Files.createDirectories(file.getParentFile().toPath());
            } catch (IOException e) {
                Logger.logException("Cant create File!", e);
            }
        }

        if(!file.exists()){
            try {
                Files.createFile(file.toPath());
            } catch (IOException e) {
                Logger.logException("Cant create File!", e);
            }
        }

        logToFile(file, toLog);
    }

    /**
     * Log error to file
     * @param toLog Error to log
     */
    public static void errorLog(final Object toLog){
        final File file = new File("Log//Wrapper//" + FILE_TIME_FORMAT.format(new Date()) + ".error");
        if(!file.getParentFile().exists()){
            try {
                Files.createDirectories(file.getParentFile().toPath());
            } catch (IOException e) {
                Logger.logException("Cant create File!", e);
            }
        }

        if(!file.exists()){
            try {
                Files.createFile(file.toPath());
            } catch (IOException e) {
                Logger.logException("Cant create File!", e);
            }
        }

        logToFile(file, toLog);
    }

    /**
     * Log an object to file
     * @param file Logfile
     * @param object Object to log
     */
    //TODO Printwriter?
    private static void logToFile(final File file, final Object object) {
        try {
            final ArrayList<String> list = (ArrayList<String>) Files.readAllLines(file.toPath());
            @Cleanup final BufferedWriter bw = Files.newBufferedWriter(file.toPath());
            for(final String s : list){
                bw.write(s);
                bw.newLine();
            }
            bw.write(object.toString());
            bw.flush();
        } catch (IOException e) {
            Logger.logException("Cant write to File!", e);
        }
    }

}
