package de.tammo.wrapper.logging;

public enum LogLevel {

    /**
     * Level to Log
     */
    INFORMATION(0),
    WARNING(1),
    ERROR(2),
    CRITICAL(3);

    LogLevel(final int level){}
}
