package de.tammo.wrapper.core;

import lombok.Getter;
import lombok.Setter;

public class MasterData {

    /**
     * Host Data from Master
     */
    @Setter
    @Getter
    private String host = "";

    /**
     * Port from Master
     */
    @Getter
    private int port = 7800;

}
