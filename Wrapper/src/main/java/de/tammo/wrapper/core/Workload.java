package de.tammo.wrapper.core;

import com.sun.management.OperatingSystemMXBean;
import de.tammo.wrapper.Wrapper;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;

public class Workload {

    private final OperatingSystemMXBean os = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();

    private final ArrayList<Usage> usage = new ArrayList<>();

    private Thread thread;

    public void start() {
        this.thread = new Thread(() -> {
            Runtime.getRuntime().addShutdownHook(new Thread(this.thread::interrupt));
            while(Wrapper.isRunning()){
                if (this.usage.size() > 10) {
                    this.usage.remove(0);
                }
                this.usage.add(new Usage(getSystemCPU(), getProcessCPU(), getRamMB()));

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }, "workload");
        this.thread.start();
    }

    public final Usage getAverageLoad() {
        int systemCpu = 0;
        int processCpu = 0;
        long ram = 0;

        for (final Usage usage : usage) {
            systemCpu += usage.getSystemCPU();
            processCpu += usage.getProcessCPU();
            ram += usage.getRamUsage();
        }

        systemCpu /= usage.size();
        processCpu /= usage.size();
        ram /= usage.size();

        return new Usage(systemCpu, processCpu, ram);
    }

    private int getSystemCPU() {
        return (int) (100 - (this.os.getSystemCpuLoad() * 100));
    }

    private int getProcessCPU() {
        return (int) (100 - (this.os.getProcessCpuLoad() * 100));
    }

    private long getRamMB() {
        return this.os.getFreePhysicalMemorySize() / (1024 * 1024);
    }
}
