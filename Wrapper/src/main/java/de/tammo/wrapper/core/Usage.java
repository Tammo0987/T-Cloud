package de.tammo.wrapper.core;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Usage {

    @Getter
    private final int systemCPU;

    @Getter
    private final int processCPU;

    @Getter
    private final long ramUsage;

}