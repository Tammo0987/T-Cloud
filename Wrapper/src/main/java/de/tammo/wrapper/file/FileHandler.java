package de.tammo.wrapper.file;

import com.google.common.reflect.ClassPath;
import de.tammo.wrapper.logging.Logger;
import lombok.Getter;

import java.io.IOException;
import java.util.ArrayList;

public class FileHandler {

    /**
     * List of all Files
     */
    @Getter
    private final ArrayList<GSONFile> files = new ArrayList<>();

    /**
     * FileHandler Constructor
     */
    public FileHandler(){
        try {
            for(final ClassPath.ClassInfo classInfo : ClassPath.from(this.getClass().getClassLoader()).getTopLevelClasses("de.tammo.wrapper.file.files")){
                this.files.add((GSONFile) Class.forName(classInfo.getName()).newInstance());
            }
        } catch (Exception e) {
            Logger.logException("Could not register File", e);
        }

        for(final GSONFile file : this.files){
            try {
                file.load();
            } catch (IOException e) {
                Logger.logException("Could not load File: " + file.toString(), e);
            }
        }
    }
}
