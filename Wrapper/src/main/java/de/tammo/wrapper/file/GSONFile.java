package de.tammo.wrapper.file;

import de.tammo.wrapper.logging.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public abstract class GSONFile {

    /**
     * Path from the File
     */
    private final File path;

    /**
     * GSONFile Constructor
     * @param name Name of the File
     */
    public GSONFile(final String name) {
        this.path = new File("Wrapper//Config//" + name + ".json");
        if(Files.notExists(this.path.toPath().getParent())){
            try {
                Files.createDirectories(this.path.toPath().getParent());
            } catch (IOException e) {
                Logger.logException("Could not create Config Directory", e);
            }
        }
        if(Files.notExists(this.path.toPath())){
            try {
                this.save();
            } catch (IOException e) {
                Logger.logException("Could not create File: " + name, e);
            }
        }
    }

    /**
     * @return a BufferedReader to load the Objects
     */
    protected BufferedReader getBufferedReader(){
        try {
            return Files.newBufferedReader(this.path.toPath());
        } catch (IOException e) {
            Logger.logException("Could not create Writer", e);
        }
        return null;
    }

    /**
     * @return a BufferedWriter to save the Objects
     */
    protected BufferedWriter getBufferedWriter(){
        try {
            return Files.newBufferedWriter(this.path.toPath());
        } catch (IOException e) {
            Logger.logException("Could not create Writer", e);
        }
        return null;
    }

    /**
     * Call to load the File
     * @throws IOException Exception
     */
    public void load() throws IOException{}

    /**
     * Call to save the File
     * @throws IOException Exception
     */
    public void save() throws IOException{}
}
