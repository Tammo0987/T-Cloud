package de.tammo.wrapper.file.files;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.core.MasterData;
import de.tammo.wrapper.file.GSONFile;
import de.tammo.wrapper.logging.Logger;
import lombok.Cleanup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public class MasterFile extends GSONFile{

    /**
     * GSONFile Constructor
     */
    public MasterFile(){
        super("master");
    }

    public void load() throws IOException{
        try {
            @Cleanup final BufferedReader br = this.getBufferedReader();
            Wrapper.setMasterData(new Gson().fromJson(br, MasterData.class));
        } catch (NumberFormatException e) {
            Logger.logException("Could not load Master Config", e);
        }
    }

    public void save() throws IOException{
        try {
            @Cleanup final BufferedWriter bw = this.getBufferedWriter();
            final Gson gson = new GsonBuilder().setPrettyPrinting().create();
            bw.write(gson.toJson(Wrapper.getMasterData()));
        } catch (NullPointerException e) {
            Logger.logException("Could not save Master Config", e);
        }
    }
}
