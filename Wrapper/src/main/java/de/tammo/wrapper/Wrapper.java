package de.tammo.wrapper;

import de.tammo.wrapper.command.CommandHandler;
import de.tammo.wrapper.core.MasterData;
import de.tammo.wrapper.core.Workload;
import de.tammo.wrapper.file.FileHandler;
import de.tammo.wrapper.file.GSONFile;
import de.tammo.wrapper.logging.Logger;
import de.tammo.wrapper.network.client.NetworkClient;
import de.tammo.wrapper.network.packet.PacketRegistry;
import de.tammo.wrapper.network.packets.ErrorPacket;
import de.tammo.wrapper.network.packets.SuccessPacket;
import de.tammo.wrapper.network.packets.WrapperConnectPacket;
import de.tammo.wrapper.network.packets.WrapperDisconnectPacket;
import de.tammo.wrapper.network.server.NetworkServer;
import de.tammo.wrapper.template.TemplateHandler;
import de.tammo.wrapper.utils.Utils;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.Scanner;

import static de.tammo.wrapper.logging.Logger.log;
import static de.tammo.wrapper.logging.Logger.logException;

public class Wrapper {

    /**
     * Instance of CommandHandler
     */
    @Getter
    private static CommandHandler commandhandler;

    /**
     * Instance of FileHandler
     */
    private static FileHandler fileHandler;

    /**
     * Instance of TemplateHandler
     */
    @Getter
    private static TemplateHandler templateHandler;

    /**
     * Instance of NetworkServer
     */
    @Getter
    private static NetworkServer server;

    /**
     * Instance of NetworkClient
     */
    @Getter
    private static NetworkClient client;

    /**
     * Instance of Workload
     */
    @Getter
    private static Workload workload;

    /**
     * Masterdate to connect to master
     */
    @Setter
    @Getter
    private static MasterData masterData;

    /**
     * Is connected with master
     */
    @Setter
    @Getter
    private static boolean connect = false;

    /**
     * Program is running
     */
    @Getter
    private static boolean running = false;

    /**
     * Wrapper constructor
     * @throws InterruptedException exception
     */
    private Wrapper() throws InterruptedException{
        running = true;

        log("Starting wrapper...");

        this.sleep(100);

        log(" _____     ____   _      _____   _   _   _____  ");
        log("|__ __|   |  __| | |    | ___ | | | | | |  __ | ");
        log("  | |     | |    | |    ||   || | | | | | |  | |");
        log("  | |     | |__  | |__  ||___|| | |_| | | |__| |");
        log("  |_|     |____| |____| |_____| |_____| |_____| ");

        log(""); this.sleep(100);
        log("© Copyright by Tammo0987 2017 - 2018 | Version 1.0-SNAPSHOT");
        this.sleep(100);
        log(""); this.sleep(100);

        this.setupInstances();

        final Scanner scanner = new Scanner(System.in);

        this.setupConnection();

        Runtime.getRuntime().addShutdownHook(new Thread(Wrapper::Shutdown));

        this.setup(scanner);

        workload.start();

        String line;
        while((line = scanner.nextLine()) != null){
            if(Utils.isStringValid(line)){
                commandhandler.executeCommand(line);
            }
        }

        scanner.close();
    }

    /**
     * main method
     *
     * @param args Java arguments
     */
    public static void main(final String[] args) {
        try {
            new Wrapper();
        } catch (InterruptedException e) {
            logException("Can't starting wrapper!", e);
        }
    }

    /**
     * Invoked when the program ends
     */
    public static void Shutdown() {
        if (!running) {
            return;
        }

        running = false;

        for (final GSONFile file : fileHandler.getFiles()) {
            try {
                file.save();
            } catch (IOException e) {
                Logger.logException("Could not save File: " + file.toString(), e);
            }
        }

        if (isConnect()) {
            client.sendPacket(new WrapperDisconnectPacket());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }

        client.disconnect();
        server.close();
        Thread.currentThread().interrupt();
        System.exit(1);
    }

    /**
     * Setup
     */
    private void setupInstances(){
        commandhandler = new CommandHandler();
        masterData = new MasterData();
        templateHandler = new TemplateHandler();
        fileHandler = new FileHandler();
        workload = new Workload();
    }

    /**
     * Starts the Setup
     *
     * @param scanner Scanner to get Input
     */
    private void setup(final Scanner scanner) {
        if (masterData.getHost().isEmpty()) {
            log("Welcome to the setup for the wrapper");
            log("Please type in the host from the master!");
            masterData.setHost(scanner.nextLine());
            log("Connect to master...");
            client = new NetworkClient(masterData.getHost(), masterData.getPort());
            client.connect();
            if(!isConnect()){
                client.sendPacket(new WrapperConnectPacket());
            }
            Logger.log("Waiting for master...");
            while(!isConnect()){
                System.out.flush();
            }
        } else {
            log("Connect to master...");
            client.connect();
            if(!isConnect()){
                client.sendPacket(new WrapperConnectPacket());
            }
            Logger.log("Waiting for master...");
            while(!isConnect()){
                System.out.flush();
            }
            Logger.log("Connected to master");
        }
    }

    /**
     * Setting up the connection to master
     */
    private void setupConnection(){
        PacketRegistry.addPacket(new SuccessPacket());
        PacketRegistry.addPacket(new ErrorPacket());
        PacketRegistry.addPacket(new WrapperConnectPacket());
        PacketRegistry.addPacket(new WrapperDisconnectPacket());

        server = new NetworkServer(7801).bind();
        client = new NetworkClient(masterData.getHost(), masterData.getPort());
    }

    /**
     * Froze the thread
     * @param ms time to wait in ms
     * @throws InterruptedException exception
     */
    private void sleep(final long ms) throws InterruptedException{
        Thread.sleep(ms);
    }
}
