package de.tammo.wrapper.template;

import de.tammo.wrapper.utils.Utils;
import lombok.*;

import java.io.File;
import java.util.ArrayList;

public class TemplateHandler {

    /**
     * Dir from Templates
     */
    private final File dir = new File("Wrapper//Templates");

    /**
     * List of all Templates
     */
    @Getter(AccessLevel.PUBLIC)
    private final ArrayList<Template> templates = new ArrayList<>();

    /**
     * TemplateHandler Constructor
     */
    public TemplateHandler(){
        if(!this.dir.exists()){
            this.dir.mkdirs();
        }
        final File global = new File("Wrapper//Global");
        if(!global.exists()){
            global.mkdirs();
        }
        this.load();
    }

    /**
     * Load all Templates
     */
    private void load(){
        this.templates.clear();
        for(final File template : this.dir.listFiles()){
            this.templates.add(new Template(template.getName()));
        }
    }

    /**
     * Add a Template
     * @param template Template
     */
    public void addTemplate(final Template template){
        this.templates.add(template);
    }

    /**
     * @param name Name from the Template
     * @return Template by Name
     */
    public final Template getTemplate(final String name){
        for(final Template template : this.templates){
            if(template.getName().equalsIgnoreCase(name)){
                return template;
            }
        }
        return null;
    }

    /**
     * Removes a Template
     * @param template Template
     */
    public void deleteTemplate(final Template template){
        this.templates.remove(template);
        Utils.deleteDir(template.getPath());
    }

}
