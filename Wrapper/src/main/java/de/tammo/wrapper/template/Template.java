package de.tammo.wrapper.template;

import de.tammo.wrapper.logging.Logger;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Template {

    @Getter
    private String name;

    public Template(final String name){
        this.name = name;
        if(!this.getPath().exists()){
            try {
                Files.createDirectories(this.getPath().toPath());
            } catch (IOException e) {
                Logger.logException("Can't create template directory!", e);
            }
        }
    }

    public final File getPath(){
        return new File("Wrapper//Templates//" + this.name);
    }
}
