package de.tammo.wrapper.utils;

import de.tammo.wrapper.logging.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

public class Utils {

    /**
     * @param line String to check
     * @return String contains only space letters
     */
    public static boolean isStringValid(final String line) {
        return line.trim().length() != 0;
    }

    /**
     * Copy a dir from from to to
     *
     * @param from from directory
     * @param to   to directory
     */
    public static void copyDir(final File from, final File to){
        if(!to.exists()) {
            try {
                Files.createDirectories(to.toPath());
            } catch (IOException e) {
                Logger.logException("Can't create directory", e);
            }
        }
        for (final File file : Objects.requireNonNull(from.listFiles())){
            if(file.isDirectory()){
                copyDir(file, new File(to.getAbsolutePath() + "//" + file.getName()));
            }else{
                try {
                    Files.copy(file.toPath(), new File(to.getAbsolutePath() + "//" + file.getName()).toPath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    Logger.logException("Error while copying files!", e);
                }
            }
        }
    }

    /**
     * Delete a dir
     * @param dir dir to delete
     */
    public static void deleteDir(final File dir) {
        for (final File file : Objects.requireNonNull(dir.listFiles())){
            if(file.isDirectory()){
                deleteDir(file);
            } else {
                try {
                    Files.delete(file.toPath());
                } catch (IOException e) {
                    Logger.logException("Can't delete file!", e);
                }
            }
        }
        try {
            Files.delete(dir.toPath());
        } catch (IOException e) {
            Logger.logException("Can't delete directory!", e);
        }
    }

}
