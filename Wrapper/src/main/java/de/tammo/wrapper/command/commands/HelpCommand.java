package de.tammo.wrapper.command.commands;

import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.command.ICommand;
import de.tammo.wrapper.logging.Logger;

@ICommand.CommandInfo(name = "help", aliases = {})
public class HelpCommand implements ICommand{

    public void execute(final String[] args){
        Logger.log("<---Help--->");
        Logger.log("");
        for(final ICommand command : Wrapper.getCommandhandler().getCommands()){
            if(command.getClass().getAnnotation(CommandInfo.class).name().equals("help")){
                continue;
            }
            Logger.log(command.getHelp());
        }
    }

    public final String getHelp(){
        return "";
    }

    public void printSyntax() {
    }
}
