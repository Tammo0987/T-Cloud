package de.tammo.wrapper.command.commands;

import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.command.ICommand;
import de.tammo.wrapper.logging.LogLevel;
import de.tammo.wrapper.logging.Logger;
import de.tammo.wrapper.template.Template;

@ICommand.CommandInfo(name = "template", aliases = {"t"})
public class TemplateCommand implements ICommand{

    public void execute(final String[] args){
        if(args.length == 1){
            if(args[0].equalsIgnoreCase("list")){
                Logger.log("<--- Templates --->");
                Logger.log("");
                Wrapper.getTemplateHandler().getTemplates().forEach(template -> Logger.log(template.getName()));
            }else{
                this.printSyntax();
            }
        }else if(args.length == 2){
            if(args[0].equalsIgnoreCase("add")){
                Wrapper.getTemplateHandler().addTemplate(new Template(args[1]));
                Logger.log("Template " + args[1] + " successfully created!");
            }else if(args[0].equalsIgnoreCase("remove")){
                final Template template = Wrapper.getTemplateHandler().getTemplate(args[1]);
                if(template != null){
                    Wrapper.getTemplateHandler().deleteTemplate(template);
                    Logger.log("Removed template " + template.getName());
                }else{
                    Logger.log("This template doesn't exists!", LogLevel.WARNING);
                }
            }else{
                this.printSyntax();
            }
        }else{
            this.printSyntax();
        }
    }

    public final String getHelp(){
        return "template";
    }

    public void printSyntax() {
        Logger.log("template list");
        Logger.log("template add <name>");
        Logger.log("template remove <name>");
    }
}
