package de.tammo.wrapper.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public interface ICommand {

    /**
     * Execute this Command
     * @param args Arguments
     */
    void execute(final String[] args);

    /**
     * @return Help for this Command
     */
    String getHelp();

    /**
     * Print the syntax
     */
    void printSyntax();

    /**
     * Annotation for Command Info
     */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    @interface CommandInfo{
        String name();
        String[] aliases();
    }
}
