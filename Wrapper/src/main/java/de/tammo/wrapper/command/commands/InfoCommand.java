package de.tammo.wrapper.command.commands;

import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.command.ICommand;
import de.tammo.wrapper.logging.Logger;

@ICommand.CommandInfo(name = "info", aliases = {"i"})
public class InfoCommand implements ICommand{

    public void execute(String[] args){
        Logger.log("Connect: " + Wrapper.isConnect() + " -> Port " + Wrapper.getMasterData().getPort());
    }

    public String getHelp(){
        return "info";
    }

    public void printSyntax() {
    }
}
