package de.tammo.wrapper.command.commands;

import de.tammo.wrapper.Wrapper;
import de.tammo.wrapper.command.ICommand;
import de.tammo.wrapper.logging.Logger;

@ICommand.CommandInfo(name = "stop", aliases = {"s", "end", "shutdown"})
public class StopCommand implements ICommand{

    public void execute(String[] args) {
        Logger.log("Stopping the wrapper...");
        Wrapper.Shutdown();
    }

    public final String getHelp(){
        return "stop";
    }

    public void printSyntax() {
    }
}
